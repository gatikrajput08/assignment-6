import styles from '../styles/Home.module.css'
import Link from "next/link";
import Image from 'next/image'


export default function CountryListItem(props) {

	return (<div>
		<div className="row justify-content-center ">
            <div className="col-md-6 col-sm-6 border mb-3 p-1">
              <div className="row justify-content-center ">
                <div className="col-md-4 col-sm-12 ">
                  {/*<!-- image -->*/}
                  <Image src={props.model?.flag} alt={props.model.name} width={600} height={400} />
                </div>
                <div className="col-md-8 col-sm-12">
                  {/*<!-- c-name -->*/}
                  <div className="row justify-content-start ">
                    <div className="col-md-6 col-sm-4">
                      <h4 className="text-left font-weight-bold">{props.model.name}</h4>
                    </div>
                  </div>
                  {/*<!-- currency -->*/}
                  <div className="row " >
                    <div className="col-12">
                      <span className={styles.spamfont } >Currency: {props.model.currencies[0].name}</span>
                    </div>
                  </div>
                  {/*<!-- date and time -->*/}
                  <div className="row ">
                    <div className="col-12">
                      <span className={styles.spamfont } > Current date and time: {dateTime(props.model.timezones[0])} </span>
                    </div>
                  </div>
                  {/*<!-- button -->*/}
                  <div className="row justify-content-start ">
                    <div className="col-md-6 col-sm-4 w-md-50 w-sm-100">
                    <Link href= {`https://www.google.com/maps/place/${props.model.name}`} >
                      <a  className={"btn btn-outline-primary rounded-0 font-weight-bold  active w-100 " + styles.showmap} role="button" >Show Map</a>
                      </Link>
                    </div>
                    <div className="col-md-6 col-sm-4 w-md-50 w-sm-100 ">
                       <Link href = {`/details/${props.model.alpha3Code.toLowerCase()}`}>
                       <a className="btn btn-outline-primary  font-weight-bold rounded-0 w-100" role="button">Details</a></Link>
                    </div>
                  </div>
                </div>
              </div>
            </div>
        </div>
        </div>
        )
} 
        




function dateTime(timezone){
  let offset1 = timezone.split("C");
  let offset2 = offset1[1].split(":");
  let offset3=parseInt(offset2[0]);
  let offset4 =parseInt(offset2[1]);
  let offset = offset3+(offset4/60);
  let x = new Date();
  let month = x.getMonth();
  let months =["Jan", "Feb", "Mar", "Apr", "May", "Jun",
    "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
  let day = x.getDate();
  let year = x.getFullYear();
  let str1 = ""
  let str=""
  let str2= ""
  str1= day+" " +months[month] + " "+ year;
  let localTime = x.getTime();
  let localOffset = x.getTimezoneOffset() * 60000;
  let utc = localTime + localOffset;
  let nd = new Date(utc + (3600000*offset));
  let h = nd.getHours();
  let m = nd.getMinutes();
  let h1="";
  let m1 ="";
  if(h<10){
    h1=h1+"0"+h.toString();
  }
  if(m<10){
    m1=m1+"0"+m.toString();
  }
  if(h<10){
    if(m<10){
      str2 = h1+":"+m1;
    }
    else{
      str2 = h1+":"+m;
    }
  }
  else{
    if(m<10){
      str2 = h+":"+m1;
    }
    else{
      str2 = h+":"+m;
    }
  }
  str=str1+" and "+str2;
  
  return(str)
}