import styles from '../../styles/Home.module.css'
import Image from 'next/image'

export async function getServerSideProps({query}){
  const {id} = query;
  const res = await fetch(`https://restcountries.eu/rest/v2/alpha/${id}`)
  const data = await res.json()
  return {
    props:{
      data
    }
  }
}

export default function details({data}) {
  let d = data.borders.map((value)=>{
  return "https://restcountries.eu/data/"+value.toLowerCase()+".svg"}
  )
  return (
  <div> 
    <div className="container">

    {/*<!-- India name -->*/}
    <div className="row justify-content-center">
      <div className="col-md-6 col-sm-12">
        <h1 >{data.name}</h1>
      </div>
    </div>
    </div>
     {/*<!-- india image and detail -->*/}
     <div >
    <div className="row justify-content-center">
      <div className="col-md-6 col-sm-12 pb-4 ">
        <div className="row">
          <div className="col-md-6 col-sm-12 ">

            {/*<!-- image -->*/}
            <Image src={data.flag} alt={data.name} width={600} height={400} className="img-fluid "/>
          </div>
          <div className="col-md-6 col-sm-12 "> 
          
            {/*<!-- c-detail-->*/}
            <div className="row justify-content-start">
              <div className="col-12">
                <span className={"text-left "+styles.fontindia}>Native Name: {data.nativeName}</span>
              </div>
            </div> 

            <div className="row justify-content-start">
              <div className="col-12">
                <span className={"text-left "+styles.fontindia}>Capital: {data.capital}</span>
              </div>
            </div>

            <div className="row justify-content-start">
              <div className="col-12">
                <span className={"text-left "+styles.fontindia}>Population: {data.population}</span>
              </div>
            </div>

            <div className="row justify-content-start">
              <div className="col-12">
                <span className={"text-left "+styles.fontindia}>Region: {data.region}</span>
              </div>
            </div>

            <div className="row justify-content-start">
              <div className="col-12">
                <span className={"text-left "+styles.fontindia}>Sub-region: {data.subregion}</span>
              </div>
            </div>

            <div className="row justify-content-start">
              <div className="col-12">
                <span className={"text-left "+styles.fontindia}>Area: {data.area} km2</span>
              </div>
            </div>

            <div className="row justify-content-start">
              <div className="col-12">
                <span className={"text-left "+styles.fontindia}>Country Code: {data.callingCodes[0]}</span>
              </div>
            </div>

            <div className="row justify-content-start">
              <div className="col-12">
                <span className={"text-left "+styles.fontindia}>Languages:{data.languages[0].name}</span>
              </div>
            </div>

            <div className="row justify-content-start">
              <div className="col-12">
                <span className={"text-left "+styles.fontindia}>Currencies: {data.currencies[0].name}</span>
              </div>
            </div>

            <div className="row justify-content-start">
              <div className="col-12">
                <span className={"text-left "+styles.fontindia}>Timezones: {data.timezones[0]}</span>
              </div>
            </div>
          </div>
        </div>
      </div>
      </div>
      </div>
   
    <div className="row justify-content-center">
        <div className="col-md-6 col-sm-12 border ">
          <h5 className="font-weight-bold">Neighbour Countries</h5>
          
            
            
              <div className="row">
              {d.map((value ,index )=>(
                <div className=" col-md-4  p-2 " key={index}>
              <Image src={value} alt="neighbourcountry" width={900} height={400} className="img-fluid "/>
                        </div>
                  ))}
              </div>
            </div>
      </div>
    </div>

    
   )
}



