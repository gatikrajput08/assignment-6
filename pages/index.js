import Head from 'next/head'
import styles from '../styles/Home.module.css'
import Image from 'next/image'
import 'bootstrap/dist/css/bootstrap.css'
import {useEffect, useState} from "react";
import CountryListItem from "../Component/CountryListItem.js";


export default function Home({countries}) {
  const [filteredCountries, setFilteredCountries] = useState(countries);
    const handleFilter = (e) => {
      const searchString = e.target.value.toLowerCase();
  const filteredCharacters  = countries.filter((country)=> {
    return(country.name.toLowerCase().includes(searchString));
  });
  setFilteredCountries(filteredCharacters)
     
    }
    
  return (
    <div> 
      <div className="container-fluid">
      
        {/*<!-- countries name -->*/}
        
        <div className="row justify-content-center">
          <div className="col-sm-12 col-md-6 p-1">
            <h1>Countries</h1>
          </div>
        </div>

        <div className="row justify-content-center">
         <div className="col-sm-12 col-md-6 p-1">
            <div className="input-group mb-3 ">
              <input type="text" onChange={handleFilter} className={"form-control rounded-0"+ styles.formcontrol} placeholder="Search countries"/>
              <div className="input-group-append">
                <span className={"input-group-text search  rounded-0"+ styles.search}>
                <Image src="/images/1.png" alt="searchbar" width={20} height={20}  />
                </span>
              </div>
            </div>
          </div>
        </div>
      </div>

      {filteredCountries.map((c, index) =>{
          return <div className='col-md-12'key={index}>
              <CountryListItem model={c} key={index} />
          </div>
      })}


      </div>

  )
}


export async function getServerSideProps(){
  const res = await fetch("https://restcountries.eu/rest/v2/all")
  const countries = await res.json()
  return {
    props:{
      countries
    }
  }
}





